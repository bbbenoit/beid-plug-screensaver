Plug/Unplug - Belgian eID Card script (deactivate/activate the screensaver)
===========================================================================

Script documentation

---

### What it does in a nutshell !

---

##### The 'good guy' card

When the card is unplugged :

1. The **_~/.eid/cacerts_** folder is emptied.
2. The screensaver is activated.
3. A successful notification is send.

When the card is plugged :

1. The screensaver is deactivated.
2. The certificates are extracted in the **_~/.eid/cacerts_** and hashed.
3. A successful notification is send.

##### The 'bad guy' card

When the card is plugged :

1. The card infos are logged in a text file (**_~/.eid/BAD_CARDS/card_number.txt_**). If the file exist, it appends to the file the date and time of a second attempt and do so for every attempt.
2. Notify (with a critical priority) that someone tried to login.
3. Exit the script

> Note : Notifications (except for the 'bad guy section') are there for development purposes. They can be removed !

---

### Prerequisite

---

- The script uses `notify-send` to push notification to the desktop and assume Cinnamon is the default window manager.

> You'll have to modify the script to suit your preferences/environment.

- PAM needs to be configured for eID Authentication. **Info** :: http://wiki.yobi.be/wiki/Belgian_eID

> **_/etc/pam_pkcs11/cacerts_** is a link to **_~/.eid/cacerts_** :exclamation:


- Create the folder structure.

> The following folders need to be created :
> - **_~/.eid_**
> - **_~/.eid/BAD_CARDS_**
> - **_~/.eid/cacerts_**

- The file **_cardnum_** needs to be created in the **_.eid_** folder. This file will contain your eID card number.

> With your card inserted, use this command :

> `eidenv | grep BELPIC_CARDNUMBER | cut -c 20- > ~/.eid/cardnum`



- Add the `card_eventmgr` command to your Startup Applications.

---

### card_eventmgr.conf example

---

> Change **_$USER_** with your username :exclamation:

```sh
# /etc/pam_pkcs11/card_eventmgr.conf

card_eventmgr {

# Run in background? Implies debug=false if set to true
daemon = true;

# show debug messages?
debug = false;

# polling time in milliseconds
timeout = 1000;
  
#
# list of events and actions

    # Card inserted
    event card_insert {
        # what to do if an action fail?
        # ignore  : continue to next action
        # return  : end action sequence
        # quit    : end program
        on_error = ignore ;

        # You can enter several, comma-separated action entries
        # they will be executed in turn
        action = "sh /home/$USER/.eid/plug.sh";
    }

    # Card has been removed

    event card_remove { 
        on_error = ignore;
        action = "sh /home/$USER/.eid/plug.sh";
    }

    # Too much time locked session

    event timeout {
    }

}
```

---

### Disclaimer

---

I made this script for my own purpose and because I wanted to play around with eID Authentication under Linux.

It has not been thought for a multi-users environment. If you can improve it or find a security issue with it, please don't hesitate to let me know !

@bbbenoit
