#!/bin/bash
#
#-------------------------------------------------------------------------------------------
#description    : Plug/Unplug - Belgian eID Card script deactivate/activate the screensaver
#-------------------------------------------------------------------------------------------
#author         : Benoit B.
#website        : https://framagit.org/bbbenoit
#email          : bbbenoit@gmx.com
#date           : 2017-02-08
#version        : v0.1
#-------------------------------------------------------------------------------------------
#note           : Check the README file
#-------------------------------------------------------------------------------------------
#

CERT="$USER.crt"

if [ ! -f $HOME/.eid/cacerts/*.crt ]; then

  CARDNUM=$(cat $HOME/.eid/cardnum)
  CARDNUM2=$(eidenv | grep BELPIC_CARDNUMBER | cut -c 20-)

  if [ "${CARDNUM2}" = "${CARDNUM}" ]; then

    cinnamon-screensaver-command -d && pkcs15-tool --read-certificate 04 > $HOME/.eid/cacerts/$CERT && pkcs15-tool --read-certificate 06 >> $HOME/.eid/cacerts/$CERT && pkcs11_make_hash_link $HOME/.eid/cacerts
    notify-send -u normal "eID identic (certificate extracted) ! :)"

  else

    DATE=$(date)
    BAD_CARDNUM=$(eidenv | grep BELPIC_CARDNUMBER | cut -c 20-)
    BAD_CARD_FILE="$HOME/.eid/BAD_CARDS/$BAD_CARDNUM.txt"
    if [ -z "$BAD_CARDNUM" ]; then
      exit 1
    elif [ ! -f $BAD_CARD_FILE ]; then
      BAD_CARD_ENV=$(eidenv)
      echo "------------------------------------" > "$BAD_CARD_FILE"
      echo "First attempt on $DATE" >> "$BAD_CARD_FILE"
      echo "------------------------------------" >> "$BAD_CARD_FILE"
      echo $BAD_CARD_ENV >> "$BAD_CARD_FILE"
      notify-send -u critical "Someone tried to login with an unauthorized eID :( .. Please check ~/.eid/BAD_CARD/$BAD_CARDNUM.txt"
    else
      echo "------------------------------------" >> "$BAD_CARD_FILE"
      echo "An other attempt on $DATE" >> "$BAD_CARD_FILE"
      notify-send -u critical "Someone with a known unauthorized eID tried to login again :( .. Please check ~/.eid/BAD_CARD/$BAD_CARDNUM.txt"
    fi
    exit 1

  fi

else

  rm $HOME/.eid/cacerts/* && cinnamon-screensaver-command -a
  notify-send -u normal "Locking the screen (certificate deleted) !"

fi
exit
